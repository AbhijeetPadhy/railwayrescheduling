/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TrainManagement;

/**
 *
 * @author abhijeet
 */
public class Platform {
    String name;
    boolean free;
    int tracks[];

    /**
     *
     * @param arg
     */
    Platform(String arg){
        String argList[] = arg.split("-");
        name = argList[0];
        tracks = new int[argList.length-1];
        for(int i=0;i<tracks.length;i++){
            tracks[i] = Integer.parseInt(argList[i+1]);
        }
        free = true;
    }
}
