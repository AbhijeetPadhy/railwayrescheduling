/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TrainManagement;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author abhijeet
 */
public class Train extends Agent {
    
    //static members
    static int count = 0;
    
    //instance members
    String name;
    String locationType;
    String location;
    Destination path[];
    
    void showTrain(){
        System.out.println("\n--------------------------Train Agent Created-----------------------------");
        System.out.println("Train "+getAID().getLocalName()+" started...");
        System.out.println("It is currently present on a "+locationType+" named "+location);
        System.out.println("The path is as follows: ");
        for(int i=0;i<path.length;i++){
            System.out.print("\n"+(i+1)+". "+path[i].name+" has arrival "+path[i].arrival+" has departure "+path[i].departure);
        }
        System.out.println("\n-------------------------------------------------------------------------\n");
    }
    
    protected void setup() {        
        String args[]=null;     
        try{
            File dir = new File(".");
            File fin = new File(dir.getCanonicalPath() + File.separator + "Dataset/Train/trains.txt");		
            FileInputStream fis = new FileInputStream(fin);
            //Construct BufferedReader from InputStreamReader
            BufferedReader br = new BufferedReader(new InputStreamReader(fis));
            String line="";
            int i=0;
            
            while (i<=count) {
                line = br.readLine();
                if(line == null)
                    break;
                args = line.split(" ");
                i++;
            }
        }catch(IOException e){
            System.out.println("Train Agent cannot be created!! "+e);
        }
        
        name = args[0];
        locationType = args[1];
        location = args[2];
        String pathList[] = args[3].substring(1,args[3].length()-1).split("-");
        path = new Destination[pathList.length];
        for(int i=0;i<path.length;i++){
            path[i] = new Destination(pathList[i]);
        }
        count++;
        
        showTrain();
        
        addBehaviour(new CyclicBehaviour(this) {
            MessageTemplate mtr=MessageTemplate.MatchPerformative(ACLMessage.INFORM);
            ACLMessage mts=new ACLMessage(ACLMessage.INFORM);
            
            @Override
            public void action() {

                ACLMessage msg=receive(mtr);
                if (msg != null) {
                    // Message received. Process it
                    String title = msg.getContent();
                    System.out.println("T "+getAID().getLocalName()+"(expected SendLocation---): "+title);
                    AID sen= msg.getSender();
                    if(title.equals("SendLocation")){
                        int k; 
                        for(k=0;k<path.length;k++){
                            if(path[k].name.equals(location.substring(1,location.length()-1).split("-")[0]))
                                break;
                        }
                        String next = path[k+1].name;
                        mts.clearAllReceiver();
                        String abc = "Location:"+name+":"+locationType+":"+location+":"+next;
                        mts.setContent(abc);
                        mts.addReceiver(sen);
                        send(mts);
                    }
                }else {
                }
            }
    });
}
}
