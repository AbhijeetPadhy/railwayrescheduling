/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TrainManagement;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author abhijeet
 */
public class TriggerAgent extends Agent {
    
    //static members
    int no=0;
    
    //instance members
    Time temp[];
    Time timeList[];
    String currentTime = "1:0:0";
    TrackConnection tracks[];
    TrackConnection temp2[];
    String reserveTrack;
    String reservePlatform;
    String reserveStation;
    String freeTrack;
    String freePlatform;
    String freeStation;
    
    protected void readTrains(){
        String args[]=null;     
        try{
            File dir = new File(".");
            File fin = new File(dir.getCanonicalPath() + File.separator + "Dataset/Train/trains.txt");		
            FileInputStream fis = new FileInputStream(fin);
            //Construct BufferedReader from InputStreamReader
            BufferedReader br = new BufferedReader(new InputStreamReader(fis));
            String line="";
            int i=0;
            
            temp = new Time[100];
            int count=0;
            while ( (line = br.readLine()) != null ) {
                args = line.split(" ");
                String name = args[0];
                String locationType = args[1];
                String location = args[2];
                String pathList[] = args[3].substring(1,args[3].length()-1).split("-");
                int POT = Integer.parseInt(args[4]);
                for(int j=0;j<pathList.length;j++){
                    
                    String args1[] = pathList[j].substring(1,pathList[j].length()-1).split(",");
                    String destName = args1[0];
                    String arrival = args1[1];
                    String departure = args1[2];
                    if(!arrival.equals("x")){
                        temp[i++] = new Time(arrival,true,name,POT);
                        count++;
                    }
                    if(!departure.equals("x")){
                        temp[i++] = new Time(departure,false,name,POT);
                        count++;
                    }
                    //System.out.println(temp[i-2].time+" "+temp[i-2].nameOfTrain+" "+temp[i-2].arrival);
                    //System.out.println(temp[i-1].time+" "+temp[i-1].nameOfTrain+" "+temp[i-1].arrival);
                }                
            }
            System.out.println(count);
            timeList = new Time[count];
            for(int j = 0;j<count;j++){
                timeList[j] = new Time();
                timeList[j].time = temp[j].time;
                timeList[j].arrival = temp[j].arrival;
                timeList[j].nameOfTrain = temp[j].nameOfTrain;
                timeList[j].completed = temp[j].completed;
                timeList[j].POT = temp[j].POT;
            }
        }catch(IOException e){
            System.out.println("TriggerAgent cannot be created!! "+e);
        }
    }

    protected void readMatrix(){
        String args[]=null;     
        try{
            File dir = new File(".");
            File fin = new File(dir.getCanonicalPath() + File.separator + "Dataset/Track/matrix.txt");		
            FileInputStream fis = new FileInputStream(fin);
            //Construct BufferedReader from InputStreamReader
            BufferedReader br = new BufferedReader(new InputStreamReader(fis));
            String line="";
            
            temp2 = new TrackConnection[100];
            int count=0;
            while ( (line = br.readLine()) != null ) {
                args = line.split(" ");
                String name = args[0];
                String s1 = args[1];
                String s2 = args[2];
                temp2[count++] = new TrackConnection(name,s1,s2);
            }
            tracks = new TrackConnection[count];
            for(int j = 0;j<count;j++){
                tracks[j] = new TrackConnection();
                tracks[j].name = temp2[j].name;
                tracks[j].s1 = temp2[j].s1;
                tracks[j].s2 = temp2[j].s2;
            }
        }catch(IOException e){
            System.out.println("TriggerAgent cannot be created!! "+e);
        }
    }
    
    protected void informDisaster(String s, String p){
        ACLMessage mts=new ACLMessage(ACLMessage.INFORM);
        mts.clearAllReceiver();
        String abc = "Disaster:"+p;
        mts.setContent(abc);
        mts.addReceiver(new AID(s,AID.ISLOCALNAME));
        send(mts);
    }
    
    protected void setup() {        
        readTrains();
        readMatrix();
        informDisaster("s2","p2");
        
        addBehaviour(new CyclicBehaviour(this) {
            MessageTemplate mtr=MessageTemplate.MatchPerformative(ACLMessage.INFORM);
            ACLMessage mts=new ACLMessage(ACLMessage.INFORM);
                        
            public int timeDiff(String a, String b){
                String aA[] = a.split(":");
                int aH = Integer.parseInt(aA[0]);
                int aM = Integer.parseInt(aA[1]);
                int aS = Integer.parseInt(aA[2]);
                
                String bA[] = b.split(":");
                int bH = Integer.parseInt(bA[0]);
                int bM = Integer.parseInt(bA[1]);
                int bS = Integer.parseInt(bA[2]);
                
                int diff = (aH-bH)*60*60 + (aM-bM)*60 + (aS-bS);
                
                return diff;
            }
            
            public String addTime(String a,int diff){
                String aA[] = a.split(":");
                long aH = Integer.parseInt(aA[0]);
                long aM = Integer.parseInt(aA[1]);
                long aS = Integer.parseInt(aA[2]);
                
                long sec = (aH)*60*60 + (aM)*60 + (aS);
                sec += diff;
                aS = sec%60;
                aM = sec/60;
                aH = aM/60;
                aM = aM%60;
                return aH+":"+aM+":"+aS;
            }
            
            public void sort(){
                int n = timeList.length;
                Time tmp;
                for(int i=0; i < n; i++){  
                    for(int j=1; j < (n-i); j++){  
                        if(timeDiff(timeList[j-1].time,timeList[j].time)>0){  
                            //swap elements  
                            tmp = timeList[j-1];  
                            timeList[j-1] = timeList[j];  
                            timeList[j] = tmp;  
                        }       
                    }  
                }  
            }
            
            //returns true if a has higher priority
            public boolean priorityCompare(Time a, Time b){
                int POA,POB;
                if(a.POT<b.POT)
                    return true;
                else 
                    return false;
            }
            
            public int prioritySort(){
                int k;
                Time tmp;
                for(k =0; k<timeList.length ; k++){
                    if(timeDiff(timeList[k].time,currentTime)>10*60)
                        break;
                }
                System.out.println("k is "+k);
                //sorts from beg of time to currentTime+10 min with highest priority at top
                for(int i=0; i < k; i++){  
                    for(int j=1; j < (k-i); j++){  
                        if(!priorityCompare(timeList[j-1],timeList[j])){  
                            //swap elements  
                            tmp = timeList[j-1];  
                            timeList[j-1] = timeList[j];  
                            timeList[j] = tmp;  
                        }       
                    }  
                }
                return k;
            } 
            
            public boolean reqSatisfied(int i){
                //if a train is on a track, and is going to arrive at a station, no need for check since
                //it has reserved for itself the platform to which it is going  in the destination station
                boolean flag=false;
                //if(timeList[i].arrival)
                //    return true;
                
                //STEP1  =  ask train for details
                mts.clearAllReceiver();
                String abc = "SendLocation";
                mts.setContent(abc);
                mts.addReceiver(new AID(timeList[i].nameOfTrain,AID.ISLOCALNAME));
                send(mts);
                
                String nameOfTrain;
                String locationType;
                String station;
                String platform;
                String next;
                String trackList[];
                String platList[];
                String track;
                
                //STEP2  =  get details from train 
                ACLMessage msg=receive(mtr);
                while(msg==null)
                    msg=receive(mtr);
                if (msg != null) {
                    
                    // Message received. Process it
                    String title = msg.getContent();
                    System.out.println("TA(expected Location--): "+title);
                    AID sen= msg.getSender();
                    if(title.split(":")[0].equals("Location")){
                        nameOfTrain = title.split(":")[1];
                        locationType = title.split(":")[2];
                        if(locationType.equals("track")){
                            track = station = title.split(":")[3].substring(1,title.split(":")[3].length()-1).split("-")[0];
                            freeTrack = track;
                            return true;
                        }
                        station = title.split(":")[3].substring(1,title.split(":")[3].length()-1).split("-")[0];
                        platform = title.split(":")[3].substring(1,title.split(":")[3].length()-1).split("-")[1];
                        next = title.split(":")[4];
                        
                        //STEP3  =  ask current station for connecting tracks
                        mts.clearAllReceiver();
                        abc = "GiveConnectingTracks:"+platform;
                        mts.setContent(abc);
                        mts.addReceiver(new AID(station,AID.ISLOCALNAME));
                        send(mts);
                        
                        //STEP4  =  get details from station 
                        msg=receive(mtr);
                        while(msg==null)
                            msg=receive(mtr);
                        if (msg != null) {
                            // Message received. Process it
                            title = msg.getContent();
                            System.out.println("TA(expected TrackList---): "+title);
                            sen= msg.getSender();
                            if(title.split(":")[0].equals("TrackList")){
                                trackList = title.split(":")[1].split(",");
                                
                                //STEP5  =  ask next station for free platform
                                mts.clearAllReceiver();
                                abc = "GiveFreePlatforms";
                                mts.setContent(abc);
                                mts.addReceiver(new AID(next,AID.ISLOCALNAME));
                                send(mts);
                                
                                //STEP6  =  get details from station 
                                msg=receive(mtr);
                                while(msg==null)
                                    msg=receive(mtr);
                                if (msg != null) {
                                    // Message received. Process it
                                    title = msg.getContent();
                                    System.out.println("TA(expected PlatList---): "+title);
                                    sen= msg.getSender();
                                    
                                    if(title.split(":")[0].equals("PlatList")){
                                        if(title.split(":")[1].equals("NA"))
                                            return false;
                                        platList = title.split(":")[1].split(",");
                                        
                                        
                                        //STEP6  =  check if connecting tracks can lead to free plat of next
                                        
                                        String plats[];
                                        for(int a=0;a<trackList.length;a++){
                                            int trackNo = Integer.parseInt(trackList[a]);
                                            if(tracks[trackNo-1].free){
                                                if(tracks[trackNo-1].s1.split("-")[0].equals(next))
                                                    plats= tracks[trackNo-1].s1.split("-");
                                                else
                                                    plats= tracks[trackNo-1].s2.split("-");
                                                for(int b = 1;b<plats.length;b++){
                                                    for(int c=0;c<platList.length;c++)
                                                        if(plats[b].equals(platList[c])){
                                                            reserveTrack = ""+trackNo;
                                                            reservePlatform = plats[b];
                                                            reserveStation = next;
                                                            freePlatform = platform;
                                                            freeStation = station;
                                                            flag = true;
                                                            break;
                                                        }
                                                    if(flag)
                                                        break;
                                                }
                                            }
                                        }
                                    }
                                }
                                
                            }
                        }else {
                        }
                        
                    }
                    
                }else {
                }
                
                return flag;
            }
            
            public void completeEvent(int i){
                
                System.out.println("Completing Event "+i);
                if(timeList[i].arrival)
                    System.out.println("Train "+timeList[i].nameOfTrain+" has arrived station "+reserveStation+" on platform "+reservePlatform+" at time "+timeList[i].time+" and has freed track no "+freeTrack);
                else
                    System.out.println("Train "+timeList[i].nameOfTrain+" has left station "+freeStation+" from platform "+freePlatform+" at time "+timeList[i].time+" and has reserved track no "+reserveTrack+" and platform "+reservePlatform+" of station "+reserveStation);
                
                //STEP 1 : change current time
                if(timeDiff(timeList[i].time,currentTime)>=0){
                    //no delay, simply increment currentTime
                    currentTime = timeList[i].time;
                }else{
                    //increment the times in timeList array
                    String trainName = timeList[i].nameOfTrain;
                    for(int a=0;a<timeList.length;a++){
                        if(timeList[a].nameOfTrain.equals(trainName) && timeList[a].completed == false)
                            timeList[a].time = addTime(timeList[a].time,timeDiff(timeList[i].time,currentTime));
                    }
                }
                
                //STEP 2 : reserve resources
                if(timeList[i].arrival){
                    //nothing to reserve
                    ;
                }else{
                    //reserve the track n plat
                    
                    //reserve track
                    for(int a=0;a<tracks.length;a++){
                        if(tracks[a].name.equals(reserveTrack)){
                            tracks[a].free = false;
                            break;
                        }
                    }
                    //reserve platform
                    mts.clearAllReceiver();
                    String abc = "Reserve:"+reserveStation+"-"+reservePlatform;
                    mts.setContent(abc);
                    mts.addReceiver(new AID(reserveStation,AID.ISLOCALNAME));
                    send(mts);
                }
                //STEP 3 : free resources
                if(timeList[i].arrival){
                    //free the track
                    for(int a=0;a<tracks.length;a++){
                        if(tracks[a].name.equals(freeTrack)){
                            tracks[a].free = true;
                            break;
                        }
                    }
                }else{
                    //free the plat
                    mts.clearAllReceiver();
                    String abc = "Free:"+freeStation+"-"+freePlatform;
                    mts.setContent(abc);
                    mts.addReceiver(new AID(freeStation,AID.ISLOCALNAME));
                    send(mts);
                }
                //STEP 4 : change train location
                String abc;
                if(timeList[i].arrival){
                    abc = "NewLocation:station:"+reserveStation+"-"+reservePlatform;
                }else{
                    abc = "NewLocation:track:"+reserveTrack;
                }
                
                //STEP 6 : Complete the event
                timeList[i].completed = true;
            }
            
            @Override
            public void action() {
                /*if(no++==0)
                    System.out.println(reqSatisfied(0));
                */
                sort();
                int k = prioritySort();
                for(int i =0;i<timeList.length;i++){
                    System.out.println(timeList[i].nameOfTrain+" "+timeList[i].arrival+" "+timeList[i].time+" "+timeList[i].POT);
                }
                
                for(int i=0;i<=k;i++){
                    if(timeList[i].completed == false && reqSatisfied(i) == true){
                        completeEvent(i);
                        break;
                    }
                }

                
            }
    });
}
}
