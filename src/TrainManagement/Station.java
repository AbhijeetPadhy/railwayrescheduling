/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author abhijeet
 */
package TrainManagement;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.core.AID;
import java.io.*;
import java.util.ArrayList;
import java.util.ListIterator;
import java.util.logging.Level;
import java.util.logging.Logger;



public class Station extends Agent {
    
    //static members
    static int count = 0;
    
    //instance members
    String name;
    int noOfPlat;
    Platform listOfPlat[];
    
    void showStation(){
        System.out.println("\n--------------------------Station Agent Created------------------------");
        System.out.println("Station "+getAID().getLocalName()+" started...");
        System.out.println("It has "+noOfPlat+" number of platforms which are as follows: ");
        for(int i=0;i<noOfPlat;i++){
            System.out.print("\n"+(i+1)+". "+listOfPlat[i].name+" connected to tracks ");
            for(int j=0;j<listOfPlat[i].tracks.length;j++){
                System.out.print(listOfPlat[i].tracks[j]+" ");
            }
        }
        System.out.println("\n-------------------------------------------------------------------------\n");
    }
    
    protected void setup() {        
        String args[]=null;     
        try{
            File dir = new File(".");
            File fin = new File(dir.getCanonicalPath() + File.separator + "Dataset/Station/StationFinal.txt");		
            FileInputStream fis = new FileInputStream(fin);
            //Construct BufferedReader from InputStreamReader
            BufferedReader br = new BufferedReader(new InputStreamReader(fis));
            String line="";
            int i=0;
            
            while (i<=count) {
                line = br.readLine();
                if(line == null)
                    break;
                args = line.split(",");
                i++;
            }
        }catch(IOException e){
            System.out.println("Station Agent cannot be created!! "+e);
        }
        
        //coordinates = args[0];
        name = args[0];
        noOfPlat = Integer.parseInt(args[1]);
        listOfPlat = new Platform[noOfPlat];
        for(int i=0;i<noOfPlat;i++){
            listOfPlat[i] = new Platform(args[2].substring(1,args[2].length()-1).split("=")[i]);
        }
        count++;
        
        showStation();
        
        addBehaviour(new CyclicBehaviour(this) {
            MessageTemplate mtr=MessageTemplate.MatchPerformative(ACLMessage.INFORM);
            ACLMessage mts=new ACLMessage(ACLMessage.INFORM);
            
            @Override
            public void action() {

                ACLMessage msg=receive(mtr);
                if (msg != null) {
                    // Message received. Process it
                    String title = msg.getContent();
                    
                    AID sen= msg.getSender();
                    //System.out.println(title);
                    if(title.split(":")[0].equals("GiveConnectingTracks")){
                        System.out.println("S "+getAID().getLocalName()+"(expected GiveConnectingTracks---): "+title);
                        int k; 
                        for(k=0;k<listOfPlat.length;k++){
                            if(listOfPlat[k].name.equals(title.split(":")[1]))
                                break;
                        }
                        String tracks="";
                        for(int i=0;i<listOfPlat[k].tracks.length;i++)
                            tracks+=listOfPlat[k].tracks[i]+",";
                        mts.clearAllReceiver();
                        String abc = "TrackList:"+tracks.substring(0,tracks.length()-1);
                        mts.setContent(abc);
                        mts.addReceiver(sen);
                        send(mts);
                    }else if(title.equals("GiveFreePlatforms")){
                        System.out.println("S "+getAID().getLocalName()+"(expected GiveFreePlatforms---): "+title);
                        String free="";
                        for(int a=0;a<listOfPlat.length;a++){
                            if(listOfPlat[a].free)
                                free+=listOfPlat[a].name+",";
                        }
                        mts.clearAllReceiver();
                        String abc;
                        if(free.equals(""))
                            abc="PlatList:NA";
                        else
                            abc = "PlatList:"+free.substring(0,free.length()-1);
                        mts.setContent(abc);
                        mts.addReceiver(sen);
                        send(mts);
                    }else if(title.split(":")[0].equals("Reserve")){
                        System.out.println("S "+getAID().getLocalName()+"(expected Reserve---): "+title);
                        String station = title.split(":")[1].split("-")[0];
                        String platform = title.split(":")[1].split("-")[1];
                        for(int a=0;a<listOfPlat.length;a++){
                            if(listOfPlat[a].name.equals(platform)){
                                listOfPlat[a].free = false;
                                break;
                            }
                        }
                    }else if(title.split(":")[0].equals("Free")){
                        System.out.println("S "+getAID().getLocalName()+"(expected Free---): "+title);
                        String station = title.split(":")[1].split("-")[0];
                        String platform = title.split(":")[1].split("-")[1];
                        for(int a=0;a<listOfPlat.length;a++){
                            if(listOfPlat[a].name.equals(platform)){
                                listOfPlat[a].free = true;
                                break;
                            }
                        }
                    }else if(title.split(":")[0].equals("Disaster")){
                        String platform = title.split(":")[1];
                        for(int a=0;a<listOfPlat.length;a++){
                            if(listOfPlat[a].name.equals(platform)){
                                listOfPlat[a].free = false;
                                break;
                            }
                        }
                    }
                }else {
                }
            }
    });
}
}