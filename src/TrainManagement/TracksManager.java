/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TrainManagement;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.core.AID;
import java.io.*;
import java.util.ArrayList;
import java.util.ListIterator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author abhijeet
 */
public class TracksManager extends Agent {
    
    //static members
    
    //instance members
    int count;
    Track listOfTracks[];
    
    void showTracks(){
        System.out.println("\n--------------------------TracksManager Agent Created------------------------");
        System.out.println("TracksManager "+getAID().getLocalName()+" started...");
        System.out.println("It has "+count+" number of tracks which are as follows: ");
        for(int i=0;i<count;i++){
            System.out.print("\n"+(i+1)+". "+listOfTracks[i].name);
        }
        System.out.println("\n-------------------------------------------------------------------------\n");
    }
    
    protected void setup() {        
        String args[]=null;     
        try{
            File dir = new File(".");
            File fin = new File(dir.getCanonicalPath() + File.separator + "Dataset/Track/Tracks.txt");		
            FileInputStream fis = new FileInputStream(fin);
            //Construct BufferedReader from InputStreamReader
            BufferedReader br = new BufferedReader(new InputStreamReader(fis));
            String line="";
            
            count = 0;
            line = br.readLine();
            args = line.split(",");
            count = args.length;
            listOfTracks = new Track[count];
        }catch(IOException e){
            System.out.println("Station Agent cannot be created!! "+e);
        }
        
        
        for(int i=0;i<count;i++){
            listOfTracks[i] = new Track(args[i]);
        }
        
        showTracks();
        
        addBehaviour(new CyclicBehaviour(this) {
            MessageTemplate mt=MessageTemplate.MatchPerformative(ACLMessage.REQUEST);
            ACLMessage mt1=new ACLMessage(ACLMessage.INFORM);
            
            @Override
            public void action() {

                ACLMessage msg=receive(mt);
                mt1.clearAllReceiver();
                if (msg != null) {
                    // Message received. Process it
                    String title = msg.getContent();
                    AID sen= msg.getSender();
                    String arr[] = title.split(",");     
                }else {
                }
            }
    });
}
}
