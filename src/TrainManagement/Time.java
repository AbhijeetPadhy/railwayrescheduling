/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TrainManagement;

/**
 *
 * @author abhijeet
 */
public class Time {
    String time;
    boolean arrival; //true for arrival, false for depart
    String nameOfTrain;
    boolean completed;
    int POT;
    Time(String time,boolean arrival, String nameOfTrain,int POT){
        this.time = time;
        this.arrival = arrival;
        this.nameOfTrain = nameOfTrain;
        this.POT = POT;
        completed = false;
    }
    Time(){
        time = "";
        arrival = false;
        nameOfTrain = "";
        POT = 0;
        completed = false;
    }
}
